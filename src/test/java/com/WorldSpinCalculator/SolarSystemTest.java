package com.WorldSpinCalculator;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import junit.framework.TestCase;

public class SolarSystemTest extends TestCase {

	public void testCalcularAlineamiento() {
		SolarSystem sistema = new SolarSystem();
		sistema.addPlaneta(new Planeta("Alpha", 1));
		assertTrue(sistema.calcularAlineamiento(360) == 361);
		sistema.addPlaneta(new Planeta("Alpha2", 1));
		assertTrue(sistema.calcularAlineamiento(360) == 361);
		sistema.addPlaneta(new Planeta("Beta", 360));
		sistema.addPlaneta(new Planeta("Gama", 180));

		assertTrue(sistema.calcularAlineamiento(0) == 1);
		assertTrue(sistema.calcularAlineamiento(179) == 1);
		assertTrue(sistema.calcularAlineamiento(180) == 2);
		assertTrue(sistema.calcularAlineamiento(359) == 2);
		assertTrue(sistema.calcularAlineamiento(360) == 3);
		assertTrue(sistema.calcularAlineamiento(360 * 2) == 5);
		assertTrue(sistema.calcularAlineamiento(360 * 3) == 7);
		assertTrue(sistema.calcularAlineamiento(360 * 4) == 9);
		sistema = new SolarSystem();
		sistema.addPlaneta(new Planeta("Alpha", 40));
		assertTrue(sistema.calcularAlineamiento(360) == 361);
		sistema.addPlaneta(new Planeta("Alpha2", 220));
		assertTrue(sistema.calcularAlineamiento(1) == 2);
		assertTrue(sistema.calcularAlineamiento(2) == 3);
		assertTrue(sistema.calcularAlineamiento(3) == 4);




	}

}
