package com.WorldSpinCalculator;

public class Planeta {
	private String nombre;
	private Integer velocidad;

	public Planeta(String nombre, Integer velocidad) {
		super();
		this.nombre = nombre;
		this.velocidad = velocidad;
	}
	public void setVelocidad(Integer velocidad) {
		this.velocidad = velocidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getVelocidad() {
		return velocidad;
	}
	
}
