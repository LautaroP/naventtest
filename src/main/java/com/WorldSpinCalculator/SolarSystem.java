package com.WorldSpinCalculator;

import java.util.ArrayList;
import java.util.List;

public class SolarSystem {
	private List<Planeta> planetas = new ArrayList<Planeta>();

	public List<Planeta> getPlanetas() {
		return planetas;
	}

	public void setPlanetas(List<Planeta> planetas) {
		this.planetas = planetas;
	}

	public void addPlaneta(Planeta planeta) {
		planetas.add(planeta);
	}

	public long calcularAlineamiento(long dia) {
		long rta = 0;
		for (; dia >= 0; dia--) {
			boolean flag = true;
			for (int i = 0; i < planetas.size() - 1; i++) {
				if (getAngulo(planetas.get(i).getVelocidad() * dia) != getAngulo(
						planetas.get(i + 1).getVelocidad() * dia)) {
					flag = false;
					break;
				}
			}
			if (flag)
				rta++;
		}
		return rta;
	}

	private long getAngulo(long angulo) {
		return angulo - (angulo / 180) * 180;
	}

}
