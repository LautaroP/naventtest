package com.WorldSpinCalculator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Hello world!
 *
 */
public class App {

	private static boolean exit = false;

	public static void main(String[] args) {
		try {
			BufferedReader bufferRead;
			do {
				SolarSystem sistema = new SolarSystem();
				sistema.addPlaneta(new Planeta("Alpha", 1));
				sistema.addPlaneta(new Planeta("Beta", 3));
				sistema.addPlaneta(new Planeta("Gama", 5));
				String dias;
				do {
					System.out.println("Ingrese cuantos dias se calculan: ");
					bufferRead = new BufferedReader(new InputStreamReader(System.in));
					dias = bufferRead.readLine();
				} while (dias.isEmpty());
				try {
					System.out.println(
							"Los Planetas se alinearon " + sistema.calcularAlineamiento(Long.parseLong(dias)) + " veces");

					System.out.println("\nIngrese 1 para salir/Otra tecla para continuar: ");
					bufferRead = new BufferedReader(new InputStreamReader(System.in));
					exit = bufferRead.readLine().equals("1");
				} catch (NumberFormatException e) {
					System.out.println("\nCantidad Incorrecta. Intente nuevamente");
				}
			} while (!exit);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
